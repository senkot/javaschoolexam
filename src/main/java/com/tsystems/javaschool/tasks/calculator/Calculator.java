package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String toCalculate = stringToExpression(statement);
        return calculate(toCalculate);
    }

    public String stringToExpression(String string) {
        String expressionString = "";
        Stack<Character> stack = new Stack<>();

        if (string == null || string == "") {
            return null;
        } else {
            for (int i = 0; i < string.length(); i++) {
                if (string.charAt(i) == ' ') continue;
                if (Character.isDigit(string.charAt(i)) || string.charAt(i) == '.') {
                    while (!isOperator(string.charAt(i))) {
                        expressionString += string.charAt(i);
                        i++;
                        if (i == string.length()) break;
                    }
                    expressionString += " ";
                    i--;
                }

                if (isOperator(string.charAt(i))) {
                    if (string.charAt(i) == '(') stack.push(string.charAt(i));
                    else if (string.charAt(i) == ')') {
                        if (!stack.empty()) {       // проверка на наличие открывающей скобки
                            char c = stack.pop();
                            try {                        // try - проверка на наличие открывающей скобки
                                while (c != '(') {
                                    expressionString += c + " ";
                                    c = stack.pop();
                                }
                                // завершение главного цикла и запись пустой строки для null результата
                            } catch (Exception e) {
                                expressionString = null;
                                break;
                            }

                        } else {                     // проверка на наличие открывающей скобки
                            expressionString = null;
                            break;                   // завершение главного цикла и запись пустой строки для null результата
                        }
                    } else {
                        if (stack.size() > 0) {
                            if (getPriority(string.charAt(i)) <= getPriority(stack.peek())) {
                                expressionString += stack.pop() + " ";
                                stack.push(string.charAt(i));
                            } else if (getPriority(string.charAt(i)) > getPriority(stack.peek())) {
                                stack.push(string.charAt(i));
                            }
                        } else if (stack.size() <= 0 || getPriority(string.charAt(i)) > getPriority(stack.peek())) {
                            stack.push(string.charAt(i));
                        }
                    }
                }
            }
            while (stack.size() > 0) {
                expressionString += stack.pop() + " ";
            }
            return expressionString;
        }
    }

    private String calculate(String input) {
        String totalResult = "";
        double result = 0;
        Stack<Double> doubleStack = new Stack<>();

        if (input == null || input == ""){
            return null;
        } else {
            for (int i = 0; i < input.length(); i++) {
                if (Character.isDigit(input.charAt(i))) {
                    String tmp = "";
                    while (input.charAt(i) != ' ' && !isOperator(input.charAt(i))) {
                        tmp += input.charAt(i);
                        i++;
                        if (i == input.length()) break;
                    }
                    try {
                        double d = Double.parseDouble(tmp);
                        doubleStack.push(d);
                        i--;
                    } catch (Exception e) {
                        totalResult = null;
                        break;
                    }
                } else if (isOperator(input.charAt(i))) {
                    try {
                        double tmp1 = doubleStack.pop();
                        double tmp2 = doubleStack.pop();
                        switch (input.charAt(i)) {
                            case '+':
                                result = tmp1 + tmp2;
                                break;
                            case '-':
                                result = tmp2 - tmp1;
                                break;
                            case '*':
                                result = tmp1 * tmp2;
                                break;
                            case '/':
                                try {
                                    int ir = (int) tmp2 / (int) tmp1;
                                } catch (ArithmeticException e) {
                                    totalResult = null;
                                    break;
                                }
                                result = tmp2 / tmp1;
                                break;
                        }
                    } catch (Exception e) {
                        totalResult = null;
                        break;
                    }
                    doubleStack.push(result);
                }
            }
            if (totalResult == null) {
                return null;
            } else {
                double rounded = result;
//                        Math.round(result * 1000) / 1000;
                int integerCheck = (int) rounded;
                if (rounded - integerCheck == 0) return "" + integerCheck;
                totalResult = "" + rounded;
                return totalResult;
            }
        }
    }

    private boolean isOperator(char c) {
        if ("+-*/()".indexOf(c) != -1) return true;
        else return false;
    }

    private byte getPriority(char c)
    {
        switch (c)
        {
            case '(': return 0;
            case ')': return 1;
            case '+': return 2;
            case '-': return 3;
            case '*':
            case '/':
                return 4;
            default: return 6;
        }
    }
}
