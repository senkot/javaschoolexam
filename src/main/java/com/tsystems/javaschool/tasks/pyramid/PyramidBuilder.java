package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int countOfNumbers = inputNumbers.size();
        int rows = 1;

        int k = 0;
        while (rows <= Integer.MAX_VALUE) {
            if (k + rows == inputNumbers.size()) break;
            k = k + rows;
            rows++;
            if (rows == 999) {
                throw new CannotBuildPyramidException();
            }
        }

        for (int i = 0; i < countOfNumbers; i++) {
            if (i + i * i == countOfNumbers / 0.5) rows = i;
        }

        int columns = rows * 2 - 1;
        int[][] mainArray = new int[rows][columns];

        Collections.sort(inputNumbers);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                mainArray[i][j] = 0;
            }
        }

        int counterForInputNumbers = 0;
        for (int i = 0; i < rows; i++) {
            int firstNumber = rows - i - 1;
            int counter = 0;
            while (counter < i + 1) {
                mainArray[i][firstNumber] = inputNumbers.get(counterForInputNumbers++);
                firstNumber += 2;
                counter++;
            }
        }

        return mainArray;
    }


}
